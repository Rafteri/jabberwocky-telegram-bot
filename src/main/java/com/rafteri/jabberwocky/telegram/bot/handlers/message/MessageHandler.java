package com.rafteri.jabberwocky.telegram.bot.handlers.message;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Message;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public interface MessageHandler {
    boolean isCommandMatching(final Message message);

    void handle(final Message message, final TelegramBot bot);
}
