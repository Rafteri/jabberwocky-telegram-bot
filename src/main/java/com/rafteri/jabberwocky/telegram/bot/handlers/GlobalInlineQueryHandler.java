package com.rafteri.jabberwocky.telegram.bot.handlers;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.InlineQuery;
import com.pengrad.telegrambot.model.request.InlineQueryResultArticle;
import com.pengrad.telegrambot.request.AnswerInlineQuery;
import com.rafteri.jabberwocky.telegram.bot.api.JabberwockyApi;
import com.rafteri.jabberwocky.telegram.bot.api.dto.JabberwockyDto;
import com.rafteri.jabberwocky.telegram.bot.handlers.callbacks.DefinitionsHandler;
import com.rafteri.jabberwocky.telegram.bot.services.ReplyMarkupsBuilder;

import java.util.regex.Pattern;

/**
 * 17.02.2021
 *
 * @author Dinar Rafikov
 */
public class GlobalInlineQueryHandler {
    private final Pattern pattern = Pattern.compile("^[а-я]+$", Pattern.CASE_INSENSITIVE);
    private final String COUNT = "30";


    public void handle(final InlineQuery inlineQuery, final TelegramBot telegramBot) {
        final String query = inlineQuery.query();
        if (!pattern.matcher(query).matches()) {
            return;
        }
        final String page = inlineQuery.offset().isBlank() ? "0" : inlineQuery.offset();
        JabberwockyApi.search(query, page, COUNT)
                .ifPresentOrElse(
                        (pageDto) -> {
                            if (pageDto.getNumberOfElements() == 0) {
                                return;
                            }
                            final InlineQueryResultArticle[] res = new InlineQueryResultArticle[pageDto.getNumberOfElements()];
                            for (int i = 0; i < res.length; i++) {
                                final JabberwockyDto dto = pageDto.getContent().get(i);
                                res[i] = new InlineQueryResultArticle(
                                        String.valueOf(dto.getId()),
                                        dto.getResult(),
                                        dto.getOption()
                                )
                                        .replyMarkup(
                                                ReplyMarkupsBuilder.buildJabberwockyReplyMarkup(
                                                        dto,
                                                        DefinitionsHandler.Action.HIDE_DEFINITIONS,
                                                        DefinitionsHandler.Action.SHOW_DEFINITIONS
                                                )
                                        );
                            }
                            telegramBot.execute(
                                    new AnswerInlineQuery(inlineQuery.id(), res)
                                            .nextOffset(String.valueOf(Long.parseLong(page) + 1))
                            );
                        },
                        // TODO: 17.02.2021 think about it, what should i do?
                        () -> {
                        }
                );
    }
}
