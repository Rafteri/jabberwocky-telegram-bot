package com.rafteri.jabberwocky.telegram.bot.services;

import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.rafteri.jabberwocky.telegram.bot.api.dto.JabberwockyDto;
import com.rafteri.jabberwocky.telegram.bot.handlers.callbacks.DefinitionsHandler;
import com.rafteri.jabberwocky.telegram.bot.handlers.callbacks.RateHandler;
import lombok.NonNull;

import static com.rafteri.jabberwocky.telegram.bot.handlers.callbacks.DefinitionsHandler.Action.HIDE_DEFINITIONS;
import static com.rafteri.jabberwocky.telegram.bot.handlers.callbacks.DefinitionsHandler.Action.SHOW_DEFINITIONS;


/**
 * 17.02.2021
 *
 * @author Dinar Rafikov
 */
public class ReplyMarkupsBuilder {

    public static InlineKeyboardMarkup buildJabberwockyReplyMarkup(@NonNull final JabberwockyDto jabberwockyDto,
                                                                   @NonNull final DefinitionsHandler.Action action,
                                                                   @NonNull final DefinitionsHandler.Action nextAction) {
        return new InlineKeyboardMarkup(
                getRateButtons(action, jabberwockyDto),
                getDefinitionButton(nextAction, jabberwockyDto)
        );
    }

    private static InlineKeyboardButton[] getRateButtons(@NonNull final DefinitionsHandler.Action action,
                                                         @NonNull final JabberwockyDto jabberwockyDto) {
        final InlineKeyboardButton[] buttons = new InlineKeyboardButton[2];
        buttons[0] = new InlineKeyboardButton("\uD83D\uDC4D " + jabberwockyDto.getPositiveRates())
                .callbackData(RateHandler.buildCallbackData(true, action, jabberwockyDto.getId()));
        buttons[1] = new InlineKeyboardButton("\uD83D\uDC4E" + jabberwockyDto.getNegativeRates())
                .callbackData(RateHandler.buildCallbackData(false, action, jabberwockyDto.getId()));
        return buttons;
    }

    private static InlineKeyboardButton[] getDefinitionButton(final DefinitionsHandler.Action action, final JabberwockyDto jabberwockyDto) {
        final InlineKeyboardButton[] buttons = new InlineKeyboardButton[1];
        if (action.equals(SHOW_DEFINITIONS)) {
            buttons[0] = new InlineKeyboardButton(SHOW_DEFINITIONS.getTitle())
                    .callbackData(DefinitionsHandler.buildCallbackData(SHOW_DEFINITIONS, jabberwockyDto.getId()));
        } else if (action.equals(HIDE_DEFINITIONS)) {
            buttons[0] = new InlineKeyboardButton(HIDE_DEFINITIONS.getTitle())
                    .callbackData(DefinitionsHandler.buildCallbackData(HIDE_DEFINITIONS, jabberwockyDto.getId()));
        } else {
            System.err.println("Shouldn't happen, check all cases - " + action);
        }
        return buttons;
    }
}
