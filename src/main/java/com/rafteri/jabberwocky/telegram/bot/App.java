package com.rafteri.jabberwocky.telegram.bot;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.rafteri.jabberwocky.telegram.bot.handlers.GlobalHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class App {
    public static Properties properties;
    public static ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) throws IOException {
        properties = loadProperties();
        final GlobalHandler globalHandler = new GlobalHandler();

        final TelegramBot telegramBot = new TelegramBot(properties.getProperty("bot.token"));

        telegramBot.setUpdatesListener(updates -> {
            updates.forEach(update -> globalHandler.handle(update, telegramBot));
            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        });

    }

    private static Properties loadProperties() throws IOException {
        final Properties properties = new Properties();
        try (final InputStream is = App.class.getClassLoader().getResourceAsStream("app.properties")) {
            properties.load(is);
        }
        return properties;
    }
}
