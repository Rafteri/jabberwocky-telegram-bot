package com.rafteri.jabberwocky.telegram.bot.handlers.callbacks;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.EditMessageText;
import com.rafteri.jabberwocky.telegram.bot.api.JabberwockyApi;
import com.rafteri.jabberwocky.telegram.bot.api.dto.JabberwockyDto;
import com.rafteri.jabberwocky.telegram.bot.services.ReplyMarkupsBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.rafteri.jabberwocky.telegram.bot.handlers.callbacks.DefinitionsHandler.Action.HIDE_DEFINITIONS;
import static com.rafteri.jabberwocky.telegram.bot.handlers.callbacks.DefinitionsHandler.Action.SHOW_DEFINITIONS;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class DefinitionsHandler implements CallbackHandler {
    public static final String ID = "1";

    @Override
    public void handle(final CallbackQuery callbackQuery, final TelegramBot bot) {
        final Data data = parseData(callbackQuery.data());
        JabberwockyApi.getById(data.jabberwockyId)
                .ifPresentOrElse(
                        dto -> handleSuccess(callbackQuery, data.actionId, dto, bot),
                        // TODO: 13.02.2021 think about it, what should i do?
                        () -> {
                        }
                );
    }

    public static void handleSuccess(final CallbackQuery callbackQuery, final Integer actionId,
                               final JabberwockyDto dto, final TelegramBot bot) {
        final String text = getOption(actionId, dto);
        final EditMessageText emt = callbackQuery.inlineMessageId() != null
                ? new EditMessageText(callbackQuery.inlineMessageId(), text)
                : new EditMessageText(
                callbackQuery.message().chat().id(),
                callbackQuery.message().messageId(),
                text
        );
        final Action action = Action.byId(actionId);
        bot.execute(emt
                .replyMarkup(
                        ReplyMarkupsBuilder.buildJabberwockyReplyMarkup(
                                dto, action, nextAction(action)
                        )
                )
                .parseMode(ParseMode.Markdown)
        );
    }

    private static Action nextAction(final Action action) {
        if (action == SHOW_DEFINITIONS) {
            return HIDE_DEFINITIONS;
        } else if (action == HIDE_DEFINITIONS) {
            return SHOW_DEFINITIONS;
        } else {
            System.err.println("Shouldn't happen, check all cases, unknown action - " + action);
            // TODO: 17.02.2021 inform admin
            return null;
        }
    }

    public static String getOption(final Integer id, final JabberwockyDto dto) {
        if (id.equals(SHOW_DEFINITIONS.getId())) {
            return dto.getOptionWithDefinitions();
        } else if (id.equals(HIDE_DEFINITIONS.getId())) {
            return dto.getOption();
        } else {
            System.err.println("Shouldn't happen, check all cases, unknown id - " + id);
            // TODO: 17.02.2021 inform admin
            return "";
        }
    }

    public static String buildCallbackData(final Action action, final Long jabberwockyId) {
        return ID + "," + action.id + "," + jabberwockyId;
    }

    private Data parseData(String data) {
        final String[] split = data.split(",");
        return new Data(split[0], Integer.parseInt(split[1]), Long.parseLong(split[2]));
    }

    @Getter
    @AllArgsConstructor
    private static class Data {
        private final String handlerId;
        private final Integer actionId;
        private final Long jabberwockyId;
    }

    @Getter
    @AllArgsConstructor
    public enum Action {
        SHOW_DEFINITIONS(0, "Show definitions"),
        HIDE_DEFINITIONS(1, "Hide definitions");

        private final int id;
        private final String title;

        public static Action byId(final int id) {
            if (id == 0) {
                return SHOW_DEFINITIONS;
            } else if (id == 1) {
                return HIDE_DEFINITIONS;
            } else {
                throw new IllegalStateException();
            }
        }
    }
}
