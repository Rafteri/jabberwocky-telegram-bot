package com.rafteri.jabberwocky.telegram.bot.handlers.message;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.request.SendMessage;
import com.rafteri.jabberwocky.telegram.bot.handlers.GlobalHandler;
import com.rafteri.jabberwocky.telegram.bot.services.BanService;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class DefaultMessageHandler implements MessageHandler {

    private final HashMap<Long, Integer> tries = new HashMap<>();
    private final List<String> answers = List.of(
            "I'm sorry, I don't understand you. Send /help to see info about my language.",
            "Excuse me, may be it was to difficult for you. Just type /help and read carefully.",
            "Really? Again? Duh, this is difficult case. Just see /help, don't be stupid",
            "What does /help look like?",
            "(flips table) What country you from?!",
            "What ain't no country I've ever heard of! They speak /help in there?",
            "/help motherfucker do you speak it!?",
            "(points gun) Say shit that I don't understand again.",
            "SAY again! And I dare you, I double dare you motherfucker! Say it one more time.",
            "I'm done. It's over. I'm not talking to you 10 minutes. Fuck off dude."
    );

    @Override
    public boolean isCommandMatching(final Message message) {
        return true;
    }

    @Override
    public void handle(final Message message, final TelegramBot bot) {
        final Long chatId = message.chat().id();
        final int tryCount = tries.compute(chatId, (k, v) -> v == null ? 0 : v + 1);
        if (tryCount % answers.size() == answers.size() - 1) {
            BanService.ban(chatId, LocalDateTime.now().plusMinutes(10));
        }
        bot.execute(new SendMessage(chatId, answers.get(tryCount % answers.size())));
    }
}
