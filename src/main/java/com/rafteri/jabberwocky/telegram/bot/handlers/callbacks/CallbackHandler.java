package com.rafteri.jabberwocky.telegram.bot.handlers.callbacks;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.CallbackQuery;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public interface CallbackHandler {
    void handle(CallbackQuery callbackQuery, TelegramBot bot);
}
