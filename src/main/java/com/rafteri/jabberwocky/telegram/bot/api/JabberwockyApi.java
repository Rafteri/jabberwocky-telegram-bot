package com.rafteri.jabberwocky.telegram.bot.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rafteri.jabberwocky.telegram.bot.App;
import com.rafteri.jabberwocky.telegram.bot.api.dto.JabberwockyDto;
import com.rafteri.jabberwocky.telegram.bot.api.dto.PageDto;
import okhttp3.*;

import java.util.Optional;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class JabberwockyApi {
    private static final OkHttpClient okHttp = new OkHttpClient();
    private static final ObjectMapper objectMapper = App.objectMapper;

    private final static TypeReference<PageDto<JabberwockyDto>> jabberwockyPageType = new TypeReference<>(){};

    public static Optional<JabberwockyDto> getRandom() {
        final Call call = okHttp.newCall(
                new Request.Builder()
                        .get()
                        .url(App.properties.getProperty("api.host") + "/jabberwocky/random")
                        .build()
        );
        return getJabberwockyDto(call);
    }

    public static Optional<JabberwockyDto> getById(final Long id) {
        final Call call = okHttp.newCall(
                new Request.Builder()
                        .get()
                        .url(App.properties.getProperty("api.host") + "/jabberwocky/" + id)
                        .build()
        );
        return getJabberwockyDto(call);
    }

    public static Optional<JabberwockyDto> rate(final Long id, final Boolean isPositive, final String userId) {
        final Call call = okHttp.newCall(
                new Request.Builder()
                        .post(RequestBody.create(null, ""))
                        .url(
                                App.properties.getProperty("api.host") + "/jabberwocky/" + id + "/rate" +
                                        "?userId=tg" + userId +
                                        "&isPositive=" + isPositive
                        )
                        .build()
        );
        return getJabberwockyDto(call);
    }

    private static Optional<JabberwockyDto> getJabberwockyDto(final Call call) {
        try (final Response response = call.execute()) {
            if (response.isSuccessful() && response.body() != null) {
                return Optional.of(objectMapper.readValue(response.body().string(), JabberwockyDto.class));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Optional.empty();
    }

    public static Optional<PageDto<JabberwockyDto>> search(final String query, final String page, final String count) {
        final Call call = okHttp.newCall(
                new Request.Builder()
                        .get()
                        .url(
                                App.properties.getProperty("api.host") + "/jabberwocky" +
                                        "?count=" + count +
                                        "&page=" + page +
                                        "&word=" + query
                        )
                        .build()
        );
        try (final Response response = call.execute()) {
            if (response.isSuccessful() && response.body() != null) {
                return Optional.of(objectMapper.readValue(response.body().string(), jabberwockyPageType));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Optional.empty();
    }

    public static Optional<String> getDefinition(final String word) {
        // todo 404 != 500 differ this cases
        final Call call = okHttp.newCall(
                new Request.Builder()
                        .get()
                        .url(App.properties.getProperty("api.host") + "/words/definition?word=" + word)
                        .build()
        );
        try (final Response response = call.execute()) {
            if (response.isSuccessful() && response.body() != null) {
                return Optional.of(response.body().string());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Optional.empty();
    }
}
