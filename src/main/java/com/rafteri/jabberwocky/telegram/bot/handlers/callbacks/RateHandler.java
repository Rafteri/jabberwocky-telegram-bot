package com.rafteri.jabberwocky.telegram.bot.handlers.callbacks;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.CallbackQuery;
import com.rafteri.jabberwocky.telegram.bot.api.JabberwockyApi;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 22.02.2021
 *
 * @author Dinar Rafikov
 */
public class RateHandler implements CallbackHandler {
    public static final String ID = "2";

    @Override
    public void handle(final CallbackQuery callbackQuery, final TelegramBot bot) {
        final Data data = parseData(callbackQuery.data());
        JabberwockyApi.rate(data.jabberwockyId, data.getRate(), String.valueOf(callbackQuery.from().id()))
                .ifPresentOrElse(
                        dto -> DefinitionsHandler.handleSuccess(callbackQuery, data.getActionId(), dto, bot),
                        // TODO: 13.02.2021 think about it, what should i do?
                        () -> {
                        }
                );
    }

    public static String buildCallbackData(final Boolean isPositive,
                                           final DefinitionsHandler.Action action, final Long jabberwockyId) {
        return ID + "," + isPositive + "," + action.getId() + "," + jabberwockyId;
    }

    private Data parseData(String data) {
        final String[] split = data.split(",");
        return new Data(split[0], Boolean.parseBoolean(split[1]), Integer.parseInt(split[2]), Long.parseLong(split[3]));
    }

    @Getter
    @AllArgsConstructor
    private static class Data {
        private final String handlerId;
        private final Boolean rate;
        private final Integer actionId;
        private final Long jabberwockyId;
    }
}
