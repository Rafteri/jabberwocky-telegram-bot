package com.rafteri.jabberwocky.telegram.bot.handlers;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Message;
import com.rafteri.jabberwocky.telegram.bot.handlers.GlobalHandler;
import com.rafteri.jabberwocky.telegram.bot.handlers.message.*;
import com.rafteri.jabberwocky.telegram.bot.services.BanService;

import java.util.ArrayList;
import java.util.List;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class GlobalMessageHandler {

    private final List<MessageHandler> handlers = new ArrayList<>();

    public GlobalMessageHandler() {
        handlers.add(new StartMessageHandler());
        handlers.add(new DefinitionMessageHandler());
        handlers.add(new RandomJabberwockyMessageHandler());
        handlers.add(new HelpMessageHandler());
        handlers.add(new DefaultMessageHandler());
    }

    public void handle(final Message message, final TelegramBot telegramBot) {
        final Long chatId = message.chat().id();
        if (BanService.isInBan(chatId)) {
            return;
        }
        for (final MessageHandler handler : handlers) {
            if (handler.isCommandMatching(message)) {
                handler.handle(message, telegramBot);
                break;
            }
        }
    }
}
