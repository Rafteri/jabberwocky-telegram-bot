package com.rafteri.jabberwocky.telegram.bot.handlers;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.CallbackQuery;
import com.rafteri.jabberwocky.telegram.bot.handlers.callbacks.CallbackHandler;
import com.rafteri.jabberwocky.telegram.bot.handlers.callbacks.DefinitionsHandler;
import com.rafteri.jabberwocky.telegram.bot.handlers.callbacks.RateHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class GlobalCallbackHandler {

    private final Map<String, CallbackHandler> handlersById = new HashMap<>();

    public GlobalCallbackHandler() {
        handlersById.put(DefinitionsHandler.ID, new DefinitionsHandler());
        handlersById.put(RateHandler.ID, new RateHandler());
    }

    public void handle(final CallbackQuery callbackQuery, final TelegramBot telegramBot) {
        final String id = callbackQuery.data().substring(0, callbackQuery.data().indexOf(","));
        if (handlersById.containsKey(id)) {
            handlersById.get(id).handle(callbackQuery, telegramBot);
        }
    }
}
