package com.rafteri.jabberwocky.telegram.bot.handlers.message;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.request.SendMessage;

import java.util.regex.Pattern;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class StartMessageHandler implements MessageHandler {
    private final Pattern pattern = Pattern.compile("^/start(@[a-z_]+bot)?$", Pattern.CASE_INSENSITIVE);
    private final String greetingMsg = "Hi, I'm Jabberwocky bot. I generate new words by combining two existing.\n" +
            "To see my features you can type /help.";

    @Override
    public boolean isCommandMatching(final Message message) {
        final String command = message.text();
        return command != null && pattern.matcher(command).matches();
    }

    @Override
    public void handle(final Message message, final TelegramBot bot) {
        bot.execute(new SendMessage(message.chat().id(), greetingMsg));
    }
}
