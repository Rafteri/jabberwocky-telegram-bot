package com.rafteri.jabberwocky.telegram.bot.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 17.02.2021
 *
 * @author Dinar Rafikov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PageDto<T> {
    private int totalPages;
    private int totalElements;
    private int numberOfElements;
    private List<T> content;
    private boolean last;
}
