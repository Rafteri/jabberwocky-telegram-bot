package com.rafteri.jabberwocky.telegram.bot.handlers.message;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.pengrad.telegrambot.request.SendMessage;

import java.util.regex.Pattern;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class HelpMessageHandler implements MessageHandler {
    private final Pattern pattern = Pattern.compile("^/help(@[a-z_]+bot)?$", Pattern.CASE_INSENSITIVE);

    private final String helpInfo = "Hi, I'm Jabberwocky bot. Here is my possibilities:\n" +
            "/random - get random jabberwocky word.\n" +
            "/definition <word> - get definition of word.\n" +
//            "/subscribe - subscribe to daily words.\n" +
//            "/unsubscribe - unsubscribe from daily words.\n" +
            "/help - get info about my possibilities.\n"
//            "/stop - I will stop talking with you.\n"
            ;

    @Override
    public boolean isCommandMatching(final Message message) {
        final String command = message.text();
        return command != null && pattern.matcher(command).matches();
    }

    @Override
    public void handle(final Message message, final TelegramBot bot) {
        bot.execute(new SendMessage(message.chat().id(), helpInfo));
    }
}
