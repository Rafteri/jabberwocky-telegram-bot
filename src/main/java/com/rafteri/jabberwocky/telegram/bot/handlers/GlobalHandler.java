package com.rafteri.jabberwocky.telegram.bot.handlers;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.InlineQuery;
import com.pengrad.telegrambot.model.Update;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class GlobalHandler {

    private final GlobalMessageHandler messageHandler;
    private final GlobalCallbackHandler callbackHandler;
    private final GlobalInlineQueryHandler inlineQueryHandler;

    public GlobalHandler() {
        messageHandler = new GlobalMessageHandler();
        callbackHandler = new GlobalCallbackHandler();
        inlineQueryHandler = new GlobalInlineQueryHandler();
    }

    public void handle(final Update update, final TelegramBot telegramBot) {
        if (update.message() != null && update.message().viaBot() == null
                && update.message().replyToMessage() == null) {
            messageHandler.handle(update.message(), telegramBot);
        } else if (update.editedMessage() != null && update.editedMessage().viaBot() == null
                && update.editedMessage().replyToMessage() == null) {
            messageHandler.handle(update.editedMessage(), telegramBot);
        } else if (update.callbackQuery() != null && update.callbackQuery().data() != null) {
            callbackHandler.handle(update.callbackQuery(), telegramBot);
        } else if (update.inlineQuery() != null) {
            final InlineQuery inlineQuery = update.inlineQuery();
            inlineQueryHandler.handle(inlineQuery, telegramBot);
        }
    }
}
