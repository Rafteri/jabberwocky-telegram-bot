package com.rafteri.jabberwocky.telegram.bot.services;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class BanService {

    private final static Map<Long, LocalDateTime> blockList = new HashMap<>();

    public static boolean isInBan(final Long chatId) {
        if (blockList.containsKey(chatId)) {
            final LocalDateTime unbanTime = blockList.get(chatId);
            if (unbanTime.isAfter(LocalDateTime.now())) {
                return true;
            } else {
                blockList.remove(chatId);
                return false;
            }
        } else {
            return false;
        }
    }

    public static void ban(long chatId, LocalDateTime unbanTime) {
        blockList.put(chatId, unbanTime);
    }
}
