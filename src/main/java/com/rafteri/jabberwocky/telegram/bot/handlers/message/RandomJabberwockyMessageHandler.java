package com.rafteri.jabberwocky.telegram.bot.handlers.message;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.SendMessage;
import com.rafteri.jabberwocky.telegram.bot.api.JabberwockyApi;
import com.rafteri.jabberwocky.telegram.bot.handlers.callbacks.DefinitionsHandler;
import com.rafteri.jabberwocky.telegram.bot.services.ReplyMarkupsBuilder;

import java.util.regex.Pattern;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class RandomJabberwockyMessageHandler implements MessageHandler {
    private final Pattern pattern = Pattern.compile("^/random(@[a-z_]+bot)?$", Pattern.CASE_INSENSITIVE);
    private static final String errorMsg = "Oopps, currently I have some issues, try later please.";

    @Override
    public boolean isCommandMatching(final Message message) {
        final String command = message.text();
        return command != null && pattern.matcher(command).matches();
    }

    @Override
    public void handle(final Message message, final TelegramBot bot) {
        JabberwockyApi.getRandom()
                .ifPresentOrElse(
                        jabberwocky -> bot.execute(
                                new SendMessage(message.chat().id(), jabberwocky.getOption())
                                        .replyMarkup(
                                                ReplyMarkupsBuilder.buildJabberwockyReplyMarkup(
                                                        jabberwocky,
                                                        DefinitionsHandler.Action.HIDE_DEFINITIONS,
                                                        DefinitionsHandler.Action.SHOW_DEFINITIONS
                                                )
                                        )
                                        .parseMode(ParseMode.Markdown)
                        ),
                        () -> {
                            bot.execute(new SendMessage(message.chat().id(), errorMsg));
                            // TODO: 13.02.2021 inform admins
                        }
                );
    }
}
