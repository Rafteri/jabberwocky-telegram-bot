package com.rafteri.jabberwocky.telegram.bot.handlers.message;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.request.SendMessage;
import com.rafteri.jabberwocky.telegram.bot.api.JabberwockyApi;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 13.02.2021
 *
 * @author Dinar Rafikov
 */
public class DefinitionMessageHandler implements MessageHandler {
    private final Pattern pattern = Pattern.compile("^/definition(@[a-z_]+bot)?\\s+(?<word>[а-я]+)$", Pattern.CASE_INSENSITIVE);
    private final String errorMsg = "Sorry, I couldn't find this word in my vocabulary.";

    @Override
    public boolean isCommandMatching(final Message message) {
        final String text = message.text();
        return text != null && pattern.matcher(text).matches();
    }

    @Override
    public void handle(final Message message, final TelegramBot bot) {
        final String text = message.text();
        final Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            final String word = matcher.group("word");
            JabberwockyApi.getDefinition(word).ifPresentOrElse(
                    definition -> bot.execute(new SendMessage(message.chat().id(), word + " - " + definition)),
                    () -> {
                        bot.execute(new SendMessage(message.chat().id(), errorMsg));
                        // TODO: 13.02.2021 inform admins if 500
                    });
        }
    }
}
